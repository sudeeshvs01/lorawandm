from subprocess import check_output
import socket
import toml
from passlib.hash import pbkdf2_sha256
from flask import *
app = Flask(__name__)
app.secret_key = "dahfaghfgsjhgfdsgfuystfgdsjh"  
with open('/etc/lorawandm/lorawanconfig.toml') as f1:
    config = toml.loads(f1.read())
SERVER_IP=config['SERVER']['HOST']
SERVER_USER=config['SERVER']['USER']
GATEWAY_IP=config['GATEWAY']['HOST']
GATEWAY_USER=config['GATEWAY']['USER']
def isOpen(ip,port):
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   try:
      s.connect((ip, int(port)))
      s.shutdown(1)
      return True
   except:
      return False

def run_command(cmd):
    try:
        result=check_output(cmd).decode("UTF-8")
        return  str(result)
    except :
        return 'ok'

def logincheck(username,password):
    if config['USER']['USERNAME']==username:
        if pbkdf2_sha256.verify(password, config['USER']['PASSWORD']):
            return True
        else:
            return False
    else:
        return False
def shutdown(ip,user):
    pi=isOpen(ip,'80')
    if pi:
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo shutdown -h now")
        return run_command(cmd)
    else:
        return "fail"
def reboot(ip,user):
    pi=isOpen(ip,'80')
    if pi:
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo reboot")
        return run_command(cmd)
    else:
        return "fail"
    
def rebootpinggv(ip,user):
    pi=isOpen(ip,'80')
    if pi:
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo systemctl restart gatewaystatus.service")
        return run_command(cmd)
    else:
        return "fail"
def rebootairq(ip,user):
    pi=isOpen(ip,'80')
    if pi:
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo systemctl restart airquality.service")
        return run_command(cmd)
    else:
        return "fail"
def rebootlorwangw(ip,user):
    pi=isOpen(ip,'80')
    if pi:
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo systemctl restart lorainabox.service")
        run_command(cmd)
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo systemctl restart chirpstack-gateway-bridge.service ")
        run_command(cmd)
        return 'done'
    else:
        return "fail"
def rebootlorwansv(ip,user):
    pi=isOpen(ip,'80')
    if pi:
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo systemctl restart chirpstack-application-server.service")
        run_command(cmd)
        s='ssh -t '+user+'@'+ip
        cmd=s.split()
        cmd.append("sudo systemctl restart chirpstack-network-server.service")
        run_command(cmd)
        return 'done'
    else:
        return "fail"


@app.route("/")
def index():
    if 'user' not in session:
        return render_template("index.html")
    else:
        return redirect(url_for('home'))    
@app.route("/home")
def home():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        return render_template("home.html")   
@app.route("/gateway")
def gateway():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        return render_template("gateway.html")    
@app.route("/server")
def server():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        return render_template("server.html")    

@app.route('/login',methods = ['POST', 'GET'])
def login():
   if request.method == 'POST':
      result = request.form
      if logincheck(result['user'],result['pass']):
          session['user'] = result['user']
          return redirect(url_for('home'))
      else:
          return redirect(url_for('index')) 

@app.route('/logout')
def logout():
   # remove the username from the session if it is there
   session.pop('user', None)
   return redirect(url_for('index'))

@app.route("/gwreboot")
def gwreboot():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        reboot(GATEWAY_IP,GATEWAY_USER)
        return redirect(url_for('gateway')) 
@app.route("/gwshutdown")
def gwshutdown():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        shutdown(GATEWAY_IP,GATEWAY_USER)
        return redirect(url_for('gateway')) 


@app.route("/gwlorareboot")
def gwlorareboot():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        rebootlorwangw(GATEWAY_IP,GATEWAY_USER)
        return redirect(url_for('gateway')) 
@app.route("/gwpingreboot")
def gwpingreboot():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        rebootpinggv(GATEWAY_IP,GATEWAY_USER)
        return redirect(url_for('gateway')) 


@app.route("/svreboot")
def svreboot():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        reboot(SERVER_IP,SERVER_USER)
        return redirect(url_for('server')) 
@app.route("/svshutdown")
def svshutdown():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        shutdown(SERVER_IP,SERVER_USER)
        return redirect(url_for('server')) 

@app.route("/svlorareboot")
def svlorareboot():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
        rebootlorwansv(SERVER_IP,SERVER_USER)
        return redirect(url_for('server')) 
@app.route("/svairreboot")
def svairreboot():
    if 'user' not in session:
        return redirect(url_for('index'))
    else:
       rebootairq(SERVER_IP,SERVER_USER)
       return redirect(url_for('server')) 






if __name__ == '__main__':
   app.run(debug = False ,host= '0.0.0.0',port=80)
