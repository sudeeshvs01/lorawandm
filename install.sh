#!/bin/bash
clear
echo "**************LoRaWAN devcie manager installer****************";
# Stop on the first sign of trouble
set -e

if [ $UID != 0 ]; then
    echo "ERROR: Operation not permitted. Forgot sudo?"
    exit 1
fi

SCRIPT_DIR=$(pwd)
cp lighttpd.conf /etc/lighttpd/lighttpd.conf
systemctl restart lighttpd.service
apt install nginx

python3 -m pip install -r requirement.txt

FILE=/etc/nginx/sites-enabled/default
if [[ -f "$FILE" ]]; then
    rm /etc/nginx/sites-enabled/default 
fi


FILE=/etc/lorawandm
if [ -d "$FILE" ]; then
  
   echo " /etc/lorawandm exist"
else
   mkdir /etc/lorawandm
fi
FILE=/usr/share/lorawandm
if [ -d "$FILE" ]; then
  
   echo "/usr/share/lorawandm exist"
else
   mkdir /usr/share/lorawandm
fi


chmod -R 777 /etc/lorawandm
chmod -R 777 /usr/share/lorawandm
cp  lorawanconfig.toml  /etc/lorawandm

cp lorawandmpass /usr/bin/
cp LoRaWAN.py /usr/share/lorawandm
cp wsgi.py    /usr/share/lorawandm
cp -Rf static /usr/share/lorawandm
cp -Rf templates /usr/share/lorawandm
cp LoraWANDM.service /lib/systemd/system/
cp lorawandm /etc/nginx/sites-available
ln -s /etc/nginx/sites-available/lorawandm /etc/nginx/sites-enabled
systemctl enable LoraWANDM.service
systemctl start LoraWANDM.service
systemctl restart nginx

echo "**************finish installation****************";


